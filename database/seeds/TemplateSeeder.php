<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TemplateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('templates')->insert([
            'name' => 'Funding Agreement',
            'description' => 'Fundwise Consulting Services Business Agreement',
            'template_id' => '9db44d23-108e-4ee6-aefa-6db87901b5a4',
            'active' => true,
        ]);
    }
}
