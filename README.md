# Laravel Vue Js




# To run locally


```
$ cp .env.example .env # make sure you have docusign environment variables filled in 
$ php artisan migrate
$ php artisan storage:link
$ php artisan serve

```

# To Test PSR

```
$ "./vendor/bin/phpcs" # will check the errors related to psr2
$ "./vendor/bin/phpcbf" # to fix
```
# To automate checking 
```
$ cp .git-hooks/pre-commit.sh .git/hooks/pre-commit
$ chmod +x .git/hooks/pre-commit
```





# Firebase


1. Create Project
2. Generate a service account key at https://console.firebase.google.com/project/[project_id]/settings/serviceaccounts/adminsdk
3. Put the key inside /keys/ folder
4. Add the file path to `FIREBASE_CREDENTIALS` to .env file


https://savvyapps.com/blog/definitive-guide-building-web-app-vuejs-firebase


