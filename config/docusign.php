<?php

return [

    /*
     |--------------------------------------------------------------------------
     | Docusign Host
     |--------------------------------------------------------------------------
     |
     | Change this to production before going live
     |
     */

    'host' => env('DOCUSIGN_BASE_URI', 'https://demo.docusign.net') . '/restapi',

    /*
     |--------------------------------------------------------------------------
     | Docusign Default Credentials
     |--------------------------------------------------------------------------
     |
     | These are the credentials that will be used if none are specified
     |
     */


    'account_id' => env('DOCUSIGN_ACCOUNT_ID'),
    'integrator_key' => env('DOCUSIGN_INTEGRATOR_KEY'),
    'aud' => env('DOCUSIGN_AUTH_SERVER'),
    'impersonated_user_guid' => env('DOCUSIGN_IMPERSONATED_USER_GUID'),
    'private_key' => env('DOCUSIGN_PRIVATE_KEY')
];
