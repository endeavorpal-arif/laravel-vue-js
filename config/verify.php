<?php

return [
    'email' => [
        'endpoint' => env('EMAIL_VALIDATION_ENDPOINT', ''),
        'access_key' => env('EMAIL_VALIDATION_ACCESS_KEY', '')
    ],


    'phone' => [
        'endpoint' => env('PHONE_VALIDATION_ENDPOINT', ''),
        'access_key' => env('PHONE_VALIDATION_ACCESS_KEY', '')
    ]
];
