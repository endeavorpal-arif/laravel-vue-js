export default {
    state:{
        post:[],
    },
    getters:{
        getAllPost(state){
            return state.post
        },

    },
    actions:{

        gelAllPost(context){
            axios.get('/post')
                .then((response)=>{
                    console.log(response.data)
                    context.commit('allpost',response.data.posts)
                })
        },
    },
    mutations:{

        allpost(state,payload){
            return state.post = payload
        },


    }
}
