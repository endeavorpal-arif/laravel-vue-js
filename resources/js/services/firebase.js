

const messaging = window.messaging


$(function () {
    navigator.serviceWorker.register('/service-worker.js') // Added in webpack mix as     .js('resources/js/services/service-worker.js','public/service-worker.js')
        .then((registration) => {
            messaging.useServiceWorker(registration);
            messaging.usePublicVapidKey(process.env.MIX_FIREBASE_VAPID_KEY); // generate at fireabse project > settings / cloud messsaging  > Web configuration > Generate key pair

            requestPermission();

            messaging.onTokenRefresh(() => {
                messaging.getToken().then((refreshedToken) => {
                    console.log('Token refreshed.', refreshedToken);

                    setTokenSentToServer(false);

                    sendTokenToServer(refreshedToken);

                    reset();


                }).catch((err) => {
                    console.log('Unable to retrieve refreshed token ', err);

                });
            });
        });
})

function reset() {

    messaging.getToken().then((currentToken) => {
        if (currentToken) {
            sendTokenToServer(currentToken);
        } else {
            console.log('No Instance ID token available. Request permission to generate one.');
            setTokenSentToServer(false);
        }
    }).catch((err) => {
        console.log('An error occurred while retrieving token. ', err);

        setTokenSentToServer(false);
    });
}



function sendTokenToServer(currentToken) {
    if (!isTokenSentToServer()) {
        console.log('Sending token to server...');
        axios
            .post('/update-fcm-token', { token: currentToken })
            .then(response => {
                console.log(response.data);
                setTokenSentToServer(true);
            })
            .catch(error => {
                console.error(error);
                setTokenSentToServer(false);
            });

        console.log(currentToken);

    } else {
        console.log('Token already sent to server so won\'t send it again ' +
            'unless it changes',currentToken);
    }
}


function isTokenSentToServer() {
    return window.localStorage.getItem('sentToServer') === '1';
}
function setTokenSentToServer(sent) {
    window.localStorage.setItem('sentToServer', sent ? '1' : '0');
}



function requestPermission() {
    console.log('Requesting permission...');
    // [START request_permission]
    Notification.requestPermission().then((permission) => {
        if (permission === 'granted') {
            console.log('Notification permission granted.');
            // TODO(developer): Retrieve an Instance ID token for use with FCM.
            reset();
        } else {
            console.log('Unable to get permission to notify.');
        }
    });
}
function deleteToken() {
    messaging.getToken().then((currentToken) => {
        messaging.deleteToken(currentToken).then(() => {
            console.log('Token deleted.');
            setTokenSentToServer(false);
        }).catch((err) => {
            console.log('Unable to delete token. ', err);
        });
        // [END delete_token]
    }).catch((err) => {
        console.log('Error retrieving Instance ID token. ', err);

    });
}

