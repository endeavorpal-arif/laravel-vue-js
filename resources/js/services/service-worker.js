

/*
* Added in webpack mix as     .js('resources/js/services/service-worker.js','public/service-worker.js')
*/
importScripts('https://www.gstatic.com/firebasejs/7.8.0/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/7.8.0/firebase-messaging.js');


firebase.initializeApp({
    apiKey: "AIzaSyDClytM4y7b4E-nH9xROt_TLJrZp_qyowo",
    authDomain: "endeavor-pal.firebaseapp.com",
    databaseURL: "https://endeavor-pal.firebaseio.com",
    projectId: "endeavor-pal",
    storageBucket: "endeavor-pal.appspot.com",
    messagingSenderId: "241925035262",
    appId: "1:241925035262:web:edbe4b3a26234c843841af"
});

const messaging = firebase.messaging();

console.log('[firebase-messaging-sw.js] test');

messaging.setBackgroundMessageHandler(function (payload) {
    console.log('[firebase-messaging-sw.js] Received background message ', payload);

    var notificationTitle = 'Background Message Title';
    var notificationOptions = {
        body: 'Background Message body.',
        icon: payload.photoURL
    };

    return self.registration.showNotification(notificationTitle,
        notificationOptions);
});



var cacheName = 'main';
var filesToCache = [

];
self.addEventListener('install', function (e) {
    console.log('[ServiceWorker] Install');
    e.waitUntil(
        caches.open(cacheName).then(function (cache) {
            console.log('[ServiceWorker] Caching app shell');
            return cache.addAll(filesToCache);
        })
    );
});
self.addEventListener('activate', event => {
    event.waitUntil(self.clients.claim());
});
self.addEventListener('fetch', event => {
    event.respondWith(
        caches.match(event.request, { ignoreSearch: true }).then(response => {
            return response || fetch(event.request);
        })
    );
});
