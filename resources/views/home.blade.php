@extends('layouts.app')

@section('content')
<div class="container">


    <div class="  ">
        <div class="p-2"></div>

        <div>
            @if (session('status'))
            <div class="alert alert-success" role="alert">
                {{ session('status') }}
            </div>
            @endif
            <number-input-component></number-input-component>

            <templates-component></templates-component>
            <div class="p-2"> </div>

            <div class="accordion" id="mainAccordian">

                <div class="card">
                    <div class="" id="headingTwo">
                        <h2 class="mb-0">
                            <button class="btn  btn-block text-left collapsed p-3" type="button" data-toggle="collapse" data-target="#collapseSent" aria-expanded="false" aria-controls="collapseSent">
                                Sent to Client
                            </button>
                        </h2>
                    </div>
                    <div id="collapseSent" class="collapse" aria-labelledby="headingTwo" data-parent="#mainAccordian">
                        <div class="card-body">
                            <documents-sent-component></documents-sent-component>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="" id="headingThree">
                        <h2 class="mb-0">
                            <button class="btn btn-block text-left collapsed p-3" type="button" data-toggle="collapse" data-target="#collapseToBeSigned" aria-expanded="false" aria-controls="collapseToBeSigned">
                                My Tickets
                            </button>
                        </h2>
                    </div>
                    <div id="collapseToBeSigned" class="collapse" aria-labelledby="headingThree" data-parent="#mainAccordian">
                        <div class="card-body">

                            <nda-component></nda-component>
                        </div>
                    </div>
                </div>
            </div>

            <div class="p-2"></div>
            <div class="p-2"></div>
        </div>

    </div>
</div>
@endsection
