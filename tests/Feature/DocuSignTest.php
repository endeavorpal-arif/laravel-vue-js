<?php

namespace Tests\Feature;

use App\Models\Template;
use App\Services\DocumentSigning\DocumentSigning;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class ExampleTest extends TestCase
{
    /**
     * tests docusign is working with correct auth and templates are loaded to the database.
     *
     * @return void
     */
    public function testTemplatePreviewURL()
    {
        $documentSigning = resolve(DocumentSigning::class);
        $url = $documentSigning->getTemplatePreviewURL(Template::all()->first()->template_id);
        $this->assertTrue($url ? true : false);
    }
}
