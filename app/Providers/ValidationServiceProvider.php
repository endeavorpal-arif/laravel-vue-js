<?php

namespace App\Providers;

use App\Services\ValidationEmail\EmailValidation;
use App\Services\ValidationPhone\PhoneValidation;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\ServiceProvider;

class ValidationServiceProvider extends ServiceProvider
{



    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
    }

    /**
     * Bootstrap services.
     *
     * @param EmailValidation $emailValidation email validation service
     * @param PhoneValidation $phoneValidation phone validation service
     * @return void
     */
    public function boot(EmailValidation $emailValidation, PhoneValidation $phoneValidation)
    {

        // email
        Validator::extend('email+', function ($attribute, $value, $parameters, $validator) use ($emailValidation) {
            return $emailValidation->validate($value);
        });
        Validator::replacer('email+', function ($message, $attribute, $rule, $parameters) {
            return str_replace($message, "The email could not be verified.", $message);
        });


        // phone
        Validator::extend('phone+', function ($attribute, $value, $parameters, $validator) use ($phoneValidation) {
            return $phoneValidation->validate($value);
        });
        Validator::replacer('phone+', function ($message, $attribute, $rule, $parameters) {
            return str_replace($message, "The phone could not be verified.", $message);
        });
    }
}
