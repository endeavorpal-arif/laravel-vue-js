<?php

namespace App\Http\Webhooks\Docusign;

use App\Models\Document;
use Illuminate\Http\Request;
use Spatie\WebhookClient\WebhookConfig;
use Spatie\WebhookClient\SignatureValidator\SignatureValidator;

class DocusignSignatureValidator implements SignatureValidator
{


    /**
     * from interface SignatureValidator
     *
     * @param Request       $request request
     * @param WebhookConfig $config  config
     * @return bool
     */
    public function isValid(Request $request, WebhookConfig $config): bool
    {

        $body = $request->getContent();
        return $this->hashIsValid($config->signingSecret, $body, $request->header($config->signatureHeaderName));
    }



    /**
     * Computes hash for docusign webhook
     *
     * @param string $secret  secret key from docusign
     * @param string $payload webhook body
     * @return string
     */
    public function computeHash($secret, $payload)
    {
        $hexHash = hash_hmac('sha256', utf8_encode($payload), utf8_encode($secret));
        $base64Hash = base64_encode(hex2bin($hexHash));
        return $base64Hash;
    }

    /**
     * Just a helper
     *
     * @param string $secret  secret key from docusign
     * @param string $payload webhook body
     * @param string $verify  the header to verify
     * @return bool
     */
    public function hashIsValid($secret, $payload, $verify)
    {
        return ($verify === self::ComputeHash($secret, $payload));
    }
}
