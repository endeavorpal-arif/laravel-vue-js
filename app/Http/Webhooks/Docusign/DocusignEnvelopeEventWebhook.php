<?php

namespace App\Http\Webhooks\Docusign;

use App\Models\Document;
use App\Models\DocumentStatus;
use App\Services\DocumentSigning\DocumentSigning;
use DocuSign\eSign\Model\EnvelopeEvent;
use DocuSign\eSign\Model\EventNotification;
use Illuminate\Support\Facades\Log;
use Spatie\WebhookClient\Models\WebhookCall;
use Spatie\WebhookClient\ProcessWebhookJob;
use Throwable;

class DocusignEnvelopeEventWebhook extends ProcessWebhookJob
{


    /**
     * @var \Spatie\WebhookClient\Models\WebhookCall
     * */
    public $webhookCall;

    /**
     * Constructor
     *
     * @param WebhookCall $webhookCall webhook call instance
     * @return mixed
     */
    public function __construct(WebhookCall $webhookCall)
    {
        $this->webhookCall = $webhookCall;
    }


    /**
     * Handle job
     *
     * @return mixed
     */
    public function handle()
    {

        Log::info('docusign webhook handle', []);
        $request = request();
        $data = $request->getContent();
        $xml = simplexml_load_string($data, 'SimpleXMLElement', LIBXML_PARSEHUGE);

        // save the updated status

        $document = Document::with('user')->where(
            'envelope_id',
            (string) $xml->EnvelopeStatus->EnvelopeID
        )->get()->first();

        self::saveDocumentStatus($document, (string) $xml->EnvelopeStatus->Status);

        if (strtolower((string) $xml->EnvelopeStatus->Status) == Document::STATUS_COMPLETED) {
            try {
                $documentSigningService = resolve(DocumentSigning::class);
                $documentSigningService->onDocumentSigned($document);
            } catch (Throwable $e) {
                Log::info('webhoook', [$e->getMessage()]);
            }
        }

        Log::info('webhook complete', []);
    }


    /**
     * Add to status update table
     *
     * @param Document $document id
     * @param string   $status   status
     * @return void
     */
    public static function saveDocumentStatus(Document $document, string $status)
    {

        Log::info('webhook envelope status', [$document->envelope_id, $status]);
        $document->update([
            'status' => $status
        ]);
        $documentStatus = new DocumentStatus();
        $documentStatus->document_id = $document->id;
        $documentStatus->status = $status;
        $documentStatus->save();
    }

    /**
     * Get the webhook registration
     *
     * @return EventNotification
     */
    public static function getWebhookRegistration()
    {
        $eventNotification = new EventNotification();
        $eventNotification->setUrl(url('/docusign-webhook'));
        $eventNotification->setLoggingEnabled('true');
        $eventNotification->setRequireAcknowledgment('true');
        $eventNotification->setUseSoapInterface('false');
        $eventNotification->setIncludeCertificateWithSoap('false');
        $eventNotification->setSignMessageWithX509Cert('false');
        $eventNotification->setIncludeDocuments('false');
        $eventNotification->setIncludeCertificateOfCompletion('false');
        $eventNotification->setIncludeEnvelopeVoidReason('true');
        $eventNotification->setIncludeHmac('true');
        $eventNotification->setIncludeTimeZone('true');
        $eventNotification->setIncludeSenderAccountAsCustomField('true');
        $eventNotification->setIncludeDocumentFields('true');

        $eventNotification->setEnvelopeEvents([
            (new EnvelopeEvent())->setEnvelopeEventStatusCode(strtolower(Document::STATUS_SENT)),
            (new EnvelopeEvent())->setEnvelopeEventStatusCode(strtolower(Document::STATUS_DELIVERED)),
            (new EnvelopeEvent())->setEnvelopeEventStatusCode(strtolower(Document::STATUS_VOIDED)),
            (new EnvelopeEvent())->setEnvelopeEventStatusCode(strtolower(Document::STATUS_DECLINED)),
            (new EnvelopeEvent())->setEnvelopeEventStatusCode(strtolower(Document::STATUS_COMPLETED))
        ]);

        return $eventNotification;
    }
}
