<?php

namespace App\Http\Controllers;

use App\Models\Document;
use App\Services\DocumentSigning\DocumentSigning;
use Illuminate\Http\Request;
use Auth;
use Bnb\Laravel\Attachments\Attachment;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class HomeController extends Controller
{


    public $documentSigning;

    /**
     * Create a new controller instance.
     *
     * @param DocumentSigning $documentSigning service
     * @return void
     */
    public function __construct(DocumentSigning $documentSigning)
    {
        $this->middleware('auth');
        $this->documentSigning = $documentSigning;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }


    /**
     * Get user notifications
     *
     * @return Notification[]
     */
    public function notifications()
    {
        return Auth::user()->notifications;
    }


    /**
     * Handle notification
     *
     * @param string $id id
     * @return mixed
     */
    public function handleOpenNotification($id)
    {
        $notification = Auth::user()->notifications->where('id', $id)->first();

        if ($notification != null) {
            Log::info('document', $notification->data);
            $document = Document::find($notification->data['id']);
            $notification->markAsRead();
            $viewUrl = $this->documentSigning->getSigningLink($document);
            return redirect()->to($viewUrl);
        }
    }

    /**
     * Handle notification
     * @param Request $request request
     * @return mixed
     */
    public function updateFCMToken(Request $request)
    {
        $token = $request->input('token');
        $user = Auth::user();
        $user->fcm_token = $token;
        $user->update([
            'fcm_token' => $token
        ]);
        return $user;
    }

    /**
     * Attachment
     *
     * @return mixed
     */
    public function attachSample()
    {
        Auth::user()->attach(Storage::get('public/sample.pdf'), [
            'disk' => 'public',
            'key' => 'combined',
        ]);
        return Auth::user();
    }


    /**
     * Attachment
     *
     * @return mixed
     */
    public function user()
    {
        return Auth::user();
    }


    /**
     * View attachement
     *
     * @param string $attachmentId attachmentId
     * @return mixed
     */
    public function viewAttachment($attachmentId)
    {
        $attachment = Attachment::where('uuid', $attachmentId)->first();
        return $attachment->output();
    }




    /**
     * Verify phone number
     *
     * @param Request $request request
     * @return bool
     */
    public function verifyPhoneNo(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'phoneNo' => 'required|phone+'
        ]);

        if ($validator->fails()) {
            return 'invalid';
        }
        return 'valid';
    }

    /**
     * Verify email
     *
     * @param Request $request request
     * @return bool
     */
    public function verifyEmail(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'email' => 'required|email+'
        ]);

        if ($validator->fails()) {
            return 'invalid';
        }
        return 'valid';
    }
}
