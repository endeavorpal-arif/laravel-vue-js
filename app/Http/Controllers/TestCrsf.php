<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TestCrsf extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Just a test for crsf check
     *
     * @return boolean
     */
    public function crsfTest()
    {
        return 'true';
    }
}
