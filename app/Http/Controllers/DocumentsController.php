<?php

namespace App\Http\Controllers;

use Auth;
use Exception;

use App\Models\Document;
use App\Models\Template;
use App\Models\User;

use App\Notifications\DocumentAttachedNotification;
use App\Services\DocumentSigning\DocumentSigning;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class DocumentsController extends Controller
{




    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


    /**
     * Sends a NDA document to a user
     *
     * @param Request         $request         laravel request object
     * @param DocumentSigning $documentSigning docusign service
     * @return Document
     */
    public function newTicket(Request $request, DocumentSigning $documentSigning)
    {

        // commented to save free quota of email validation

        // $validator = Validator::make($request->all(), [
        //     'email' => 'required|email+',
        //     'templateName' => 'required',
        //     'templateId' => 'required'
        // ]);

        // if ($validator->fails()) {
        //     return response([
        //         'success' => false,
        //         'message' => 'Invalid Request',
        //         'errors' => $validator->errors(),
        //     ], 400);
        // }

        $templateId = $request->input('templateId');
        $templateName = $request->input('templateName');
        $email = $request->input('email');
        $client = User::where('email', $email)->first();
        $company = Auth::user();



        $document = new Document();
        $document->name = "$templateName-$company->name-$client->name";
        $document->user_id = $client->id;
        $document->company_id = $company->id;
        $document->status = '-';

        $envelope = $documentSigning->sendDocument($templateId, $client);
        $document->envelope_id = $envelope->envelopeId;
        $document->save();

        // notify
        $client->notify(new DocumentAttachedNotification($document));
        return $document;
    }


    /**
     * Return all documents for a user
     *
     * @return \App\Models\Document[]
     */
    public function allDocuments()
    {
        $documents = Document::with('user')->with('attachments')->where('user_id', Auth::user()->id)->get();

        return $documents;
    }


    /**
     * Return all documents this user has sent
     *
     * @return \App\Models\Document[]
     */
    public function documentsSent()
    {
        $documents = Document::with('user')->with('attachments')->where('company_id', Auth::user()->id)->get();
        return $documents;
    }

    /**
     * Return all available templates
     *
     * @return \App\Models\Document[]
     */
    public function allTemplates()
    {
        return Template::all();
    }



    /**
     * Go to template preview
     *
     * @param string          $templateId      t
     * @param DocumentSigning $documentSigning docusign service
     * @return mixed
     */
    public function previewTemplate($templateId, DocumentSigning $documentSigning)
    {
        return redirect()->to($documentSigning->getTemplatePreviewURL($templateId));
    }



    /**
     * Go to document signing
     *
     * @param string          $documentId      id of document entry on database
     * @param DocumentSigning $documentSigning docusign service
     * @return mixed
     */
    public function goToDocumentSigning($documentId, DocumentSigning $documentSigning)
    {
        return redirect()->to($documentSigning->getSigningLink(Document::find($documentId)));
    }
}
