<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Bnb\Laravel\Attachments\HasAttachment;

class Document extends Model
{
    use HasAttachment;

    public const STATUS_SENT = 'Sent';
    public const STATUS_DELIVERED = 'Delivered';
    public const STATUS_COMPLETED = 'Completed';
    public const STATUS_DECLINED = 'Declined';
    public const STATUS_VOIDED = 'Voided';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'status'
    ];

    /**
     * relation
     *
     * @return mixed
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }


    /**
     * relation
     *
     * @return mixed
     */
    public function company()
    {
        return $this->belongsTo(User::class);
    }
}
