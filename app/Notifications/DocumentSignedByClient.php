<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use NotificationChannels\Fcm\FcmChannel;
use NotificationChannels\Fcm\FcmMessage;

class DocumentSignedByClient extends Notification implements ShouldQueue
{
    use Queueable;

    private $document;

    /**
     * Create a new notification instance.
     *
     * @param Document $document d
     * @return void
     */
    public function __construct($document)
    {
        $this->document = $document;
        //
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed $notifiable n
     * @return array
     */
    public function via($notifiable)
    {
        return [FcmChannel::class, 'database']; //'mail',
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed $notifiable n
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->line('The introduction to the notification.')
            ->action('Notification Action', route('home'))
            ->line('Thank you for using our application!');
    }

    /**
     *
     * @param  mixed $notifiable n
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toFcm($notifiable)
    {
        $document = $this->document;
        return FcmMessage::create()
            ->setData([
                'id' => $document->id.'',
                'name' => $this->document->name,
            ])
            ->setNotification(\NotificationChannels\Fcm\Resources\Notification::create()
                ->setTitle('Client Signed')
                ->setBody('A document '.$document->name.' has been signed by the client.')
                ->setImage(asset('/storage/document-1.png')));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed $notifiable n
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'id' => $this->document->id,
            'name' => $this->document->name,
        ];
    }

    /**
     * Get the database representation of the notification.
     *
     * @param  mixed $notifiable n
     * @return array
     */
    public function toDatabase($notifiable)
    {
        return [
            'id' => $this->document->id,
            'name' => $this->document->name,
        ];
    }

    /**
     * Get the type of the notification being broadcast.
     *
     * @return string
     */
    public function broadcastType()
    {
        return 'notification.document-attached';
    }
}
