<?php

namespace App\Services\DocumentSigning;

use DocuSign\eSign\Client\ApiClient;
use DocuSign\eSign\Configuration;
use Illuminate\Contracts\Support\DeferrableProvider;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\ServiceProvider;

class DocuSignServiceProvider extends ServiceProvider implements DeferrableProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {

        $docuSign = config('docusign');
        $config = (new Configuration())->setHost($docuSign['host']);
        $config->addDefaultHeader('X-DocuSign-Authentication', json_encode([
            'IntegratorKey' =>  $docuSign['integrator_key']
        ]));
        $client = new ApiClient($config);

        $this->app->singleton(DocumentSigning::class, function ($app) use ($client) {
            return new DocusignService($client);
        });
    }

    /**
     * Bootstrap services.
     *
     * @param DocumentSigning $documentSigning docusign service
     * @return void
     */
    public function boot(DocumentSigning $documentSigning)
    {
        $documentSigning->init();
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [DocumentSigning::class];
    }
}
