<?php

namespace App\Services\DocumentSigning;

use App\Models\Document;
use App\Http\Webhooks\Docusign\DocusignEnvelopeEventWebhook;
use App\Notifications\DocumentSignedByClient;
use App\Models\User;
use App\Services\DocumentSigning\DocumentSigning;
use Auth;
use DocuSign\eSign\Api\EnvelopesApi;
use DocuSign\eSign\Api\TemplatesApi;
use DocuSign\eSign\Client\ApiClient;
use DocuSign\eSign\Model\EnvelopeDefinition;
use DocuSign\eSign\Model\RecipientPreviewRequest;
use DocuSign\eSign\Model\RecipientViewRequest;
use DocuSign\eSign\Model\TemplateRole;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

class DocusignService implements DocumentSigning
{


    /**
     * Docusign apiclient
     */
    public $apiClient;

    /**
     * Create a new controller instance.
     *
     * @param ApiClient $apiClient docusign api client
     * @return void
     */
    public function __construct(ApiClient $apiClient)
    {
        $this->apiClient  = $apiClient;
        $this->accountId = config('docusign.account_id');
    }


    /**
     * init
     *
     * @return void
     */
    public function init()
    {
        $auth = new DocusignAuth($this->apiClient);
        $auth->refreshAuthToken();
    }


    /**
     * Sends a document to a template based on template id
     *
     * @param string $templateId Template ID to be used to create envelope
     * @param User   $user       user The user the document will be sent
     * @return array
     */
    public function sendDocument(string $templateId, User $user)
    {

        // create envelope object
        $envelopeDefinition = new EnvelopeDefinition([
            'email_subject' => 'Please sign this document',
            'status' => 'sent'
        ]);
        $envelopeDefinition->setTemplateId($templateId);
        $envelopeDefinition->setTemplateRoles([
            new TemplateRole([
                'email' => $user->email, 'name' => $user->name,
                'role_name' => 'Client'
            ]),
            new TemplateRole([
                'email' => Auth::user()->email,
                'name' => Auth::user()->name,
                'role_name' => 'cc'
            ])
        ]);
        $envelopeDefinition->setEventNotification(DocusignEnvelopeEventWebhook::getWebhookRegistration());

        // save envelope
        $envelopeApi = new EnvelopesApi($this->apiClient);
        $envelope = $envelopeApi->createEnvelope($this->accountId, $envelopeDefinition);
        Log::info('envelop created', [$envelope]);
        return json_decode($envelope);
    }



    /**
     * Create recipient view
     *
     * @param Document $document d
     * @return string
     */
    public function getSigningLink(Document $document)
    {
        $envelopeApi = new EnvelopesApi($this->apiClient);
        $user = Auth::user();
        $req = new RecipientViewRequest();
        $req->setEmail($user->email);
        $req->setUserName($user->name);
        // $req->setClientUserId($user->id);
        $req->setAuthenticationMethod('Email');
        $req->setReturnUrl(route('home'));
        Log::info('envelopeid', [$document]);
        $url =  $envelopeApi->createRecipientView($this->accountId, $document->envelope_id, $req);
        Log::info('url', [$url]);
        return $url['url'];
    }



    /**
     * Downloads the combined pdf from docusign and attaches to the document
     *
     * @param Document $document document
     * @return void
     */
    public function onDocumentSigned(Document $document)
    {

        $envelopeId = $document->envelope_id;
        $envelopeApi = new EnvelopesApi($this->apiClient);
        $file = $envelopeApi->getDocument($this->accountId, 'combined', $envelopeId);
        $filePath = $file->getRealPath();
        $file = null;
        $fileSource = fopen($filePath, 'r+');
        $document->attach($fileSource, [
            'disk' => 'public',
            'key' => 'combined',
            'filename' => $document->name . '.pdf'
        ]);
        $document->update([
            'status' => Document::STATUS_COMPLETED
        ]);

        $company = User::where('id', $document->company_id)->first();
        $company->notify(new DocumentSignedByClient($document));
    }



    /**
     * Downloads a combined pdf of the envelope
     *
     * @param string $envelopeId envelopeId
     * @param string $path       location to save the file
     * @return void
     */
    public function downloadEnvelopePdf($envelopeId, $path)
    {
        $envelopeApi = new EnvelopesApi($this->apiClient);
        $file = $envelopeApi->getDocument($this->accountId, 'combined', $envelopeId);
        Storage::putFileAs('public', $file, $path);
    }


    /**
     * On getAllTemplates
     *
     * @return any[]
     */
    public function getAllTemplates()
    {
        $templateAPI = new TemplatesApi($this->apiClient);

        return $templateAPI->listTemplates($this->accountId);
    }



    /**
     * get template preview url
     *
     * @param string $templateId Template ID
     * @return any[]
     */
    public function getTemplatePreviewURL($templateId)
    {
        $templateAPI = new TemplatesApi($this->apiClient);
        $options = new RecipientPreviewRequest();
        $options->setReturnUrl(url('/'));
        return $templateAPI->createTemplateRecipientPreview(
            $this->accountId,
            $templateId,
            $options
        )['url'];
    }
}
