<?php

namespace App\Services\DocumentSigning;

use App\Models\Document;
use App\Models\User;

interface DocumentSigning
{



    /**
     * init
     *
     * @return void
     */
    public function init();



    /**
     * return templates from docusign
     *
     * @return any[]
     */
    public function getAllTemplates();


    /**
     * get templeate preivew url
     *
     * @param string $templateId template Id
     * @return any[]
     */
    public function getTemplatePreviewURL($templateId);


    /**
     * Sends a NDA document to a user
     *
     * @param string $templateId template
     * @param User   $user       user
     * @return mixed
     */
    public function sendDocument(string $templateId, User $user);

    /**
     * Create recipient view
     *
     * @param Document $document d
     * @return string
     */
    public function getSigningLink(Document $document);


    /**
     * Downloads the combined pdf from docusign and attaches to the document
     *
     * @param Document $document document
     * @return void
     */
    public function onDocumentSigned(Document $document);
}
