<?php

/**
 *
 *
 * For demo please give consent manually on this url
 * https://account-d.docusign.com/password?response_type=code&scope=signature%20impersonation&client_id=09d92a6d-9a4d-4354-9571-991b87eecc45&redirect_uri=https%3A%2F%2F2ca35514f9f7.ap.ngrok.io%2F#/challenge/appconsent
 *
 *
 * Note change the redirect uri
 *
 *
 */

namespace App\Services\DocumentSigning;

use DocuSign\eSign\Client\ApiClient;
use \Firebase\JWT\JWT;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;

class DocusignAuth
{

    const TOKEN_EXPIRATION_IN_SECONDS = 3600; // 1 hour
    const TOKEN_REPLACEMENT_IN_SECONDS = 3000; // 50 minutes



    protected $apiClient;
    private $config;

    /**
     * Construct
     *
     * @param ApiClient $apiClient docusign Client
     * @return DocusignAuth
     */
    public function __construct(ApiClient $apiClient)
    {
        $this->apiClient = $apiClient;
        $this->config = config('docusign');
    }

    /**
     * update authtoken in the apiClient config.
     * @return void
     */
    public function refreshAuthToken()
    {
        $jwtResponse = $this->getToken();
        $config = $this->apiClient->getConfig();
        $config->setAccessToken($jwtResponse->access_token);
        $config->addDefaultHeader('Authorization', 'Bearer ' . $jwtResponse->access_token);
    }



    /**
     * retrieves token from cache if present otherwise make a call for a new one.
     * @return mixed
     */
    protected function getToken()
    {
        if (Cache::store('redis')->has('docusign_access_token')) {
            $fromCache = json_decode(Cache::store('redis')->get('docusign_access_token'));
            Log::info('token from cache', [$fromCache->access_token]);
            return $fromCache;
        }
        $accessToken  = $this->requestNewAuthToken();
        Log::info('new token', [$accessToken->access_token]);
        Cache::store('redis')->put(
            'docusign_access_token',
            json_encode($accessToken),
            self::TOKEN_REPLACEMENT_IN_SECONDS
        );
        return $accessToken;
    }



    /**
     * creates and signs jwt token
     * @return string
     */
    private function makeJwtToken()
    {

        $current_time = time();
        $aud = $this->config['aud'];
        $iss = $this->config['integrator_key'];

        $token = array(
            "iss" => $iss,
            "sub" => $this->config['impersonated_user_guid'],
            "aud" => $aud,
            "scope" => 'signature impersonation',
            "nbf" => $current_time,
            "exp" => $current_time + 55 * 1000
        );
        $private_key = $this->config['private_key'];
        $jwt = JWT::encode($token, $private_key, 'RS256');
        return $jwt;
    }



    /**
     * request an access token for jwt token
     * @return mixed
     */
    private function requestNewAuthToken()
    {
        $headers = [
            'Accept' => 'application/json',
            'Content-Type' => 'application/json',
        ];
        $data = [
            'grant_type' => 'urn:ietf:params:oauth:grant-type:jwt-bearer',
            'assertion' => $this->makeJwtToken()
        ];
        $aud = $this->config['aud'];
        $client = new \GuzzleHttp\Client();
        $response = $client->post(
            "https://{$aud}/oauth/token",
            [
                'headers' => $headers,
                'body' => json_encode($data)
            ]
        );


        // Handle the response if it is an html page
        if (strpos($response->getBody(), '<html>') !== false) {
            throw new Exception("An error response was received!\n\n");
        }

        $response = (string) $response->getBody();
        $json = json_decode($response);

        if (property_exists($json, 'error') and $json->{'error'} == 'consent_required') {
            $consent_url = 'https://{aud}/oauth/auth?response_type=code&scope={this->permission_scopes}'
                . '&client_id={iss}&redirect_uri={this->redirect_uri}';
            throw new Exception("\n\nC O N S E N T   R E Q U I R E D\n"
                . "Ask the user who will be impersonated to run the following url:\n"
                . "    {$consent_url}\n"
                . "It will ask the user to login and to approve access by your application.\n\n"
                . "Alternatively, an Administrator can use Organization Administration to\n"
                . "pre-approve one or more users.\n\n", 401);
        }

        if (property_exists($json, 'error') or !property_exists($json, 'access_token')) {
            throw new Exception("\n\nUnexpected error: {$json->{'error'}}\n\n");
        }
        return $json;
    }
}
