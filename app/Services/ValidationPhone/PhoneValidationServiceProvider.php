<?php

namespace App\Services\ValidationPhone;

use Carbon\Laravel\ServiceProvider;

class PhoneValidationServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {

        $this->app->bind(PhoneValidation::class, function ($app) {
            return new PhoneValidationService();
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
