<?php

namespace App\Services\ValidationPhone;

use App\Services\ValidationPhone\PhoneValidation;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

class PhoneValidationService implements PhoneValidation
{




    /**
     * Phone validation endpoint
     */
    public $endpoint;

    /**
     * Phone validation endpoint
     */
    public $accessKey;

    /**
     * Create a new service instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->endpoint = config('verify.phone.endpoint');
        $this->accessKey = config('verify.phone.access_key');
    }



    /**
     * validate phone
     *
     * @param string $phoneNo phoneNo
     * @return mixed
     */
    public function validate($phoneNo)
    {

        $client = new \GuzzleHttp\Client();
        $request = $client->get(
            $this->endpoint
                . '?access_key=' . $this->accessKeyF
                . '&number=' . $phoneNo . '&country_code='
        );
        $response = (string) $request->getBody();
        Log::info('http phone validation', [$response]);
        $response = json_decode($response);
        return $response->valid;
    }
}
