<?php

namespace App\Services\ValidationPhone;

interface PhoneValidation
{



    /**
     * validate phoneNo
     *
     * @param string $phoneNo phoneNo
     * @return mixed
     */
    public function validate($phoneNo);
}
