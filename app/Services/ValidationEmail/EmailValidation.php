<?php

namespace App\Services\ValidationEmail;

interface EmailValidation
{



    /**
     * validated email
     *
     * @param string $email email
     * @return mixed
     */
    public function validate($email);
}
