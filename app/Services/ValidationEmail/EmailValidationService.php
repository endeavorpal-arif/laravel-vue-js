<?php

namespace App\Services\ValidationEmail;

use App\Services\ValidationEmail\EmailValidation;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

class EmailValidationService implements EmailValidation
{




    /**
     * Email validation endpoint
     */
    public $endpoint;

    /**
     * Email validation endpoint
     */
    public $accessKey;

    /**
     * Create a new service instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->endpoint = config('verify.email.endpoint');
        $this->accessKey = config('verify.email.access_key');
    }

    /**
     * validated email
     *
     * @param string $email email
     * @return mixed
     */
    public function validate($email)
    {
        $client = new \GuzzleHttp\Client();
        $request = $client->get(
            $this->endpoint
                . '?access_key=' . $this->accessKey
                . '&email=' . $email . '&smtp=1'
        );
        $response = (string) $request->getBody();
        Log::info('http email validation', [$response]);
        $response = json_decode($response);
        return !$response->did_you_mean  && $response->mx_found;
    }
}
