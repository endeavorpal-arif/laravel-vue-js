<?php

namespace App\Services\ValidationEmail;

use Carbon\Laravel\ServiceProvider;

class EmailValidationServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {

        $this->app->bind(EmailValidation::class, function ($app) {
            return new EmailValidationService();
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
