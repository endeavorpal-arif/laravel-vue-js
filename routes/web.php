<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::post('/update-fcm-token', 'HomeController@updateFCMToken')->name('update-fcm-token');
Route::post('/crsf-test', 'TestCrsf@crsfTest');

Route::get('/notifications', 'HomeController@notifications')->name('notifications_list');
Route::get('/view/notification/{id}', 'HomeController@handleOpenNotification')->name('handle_notification');

Route::get('/documents-sent', 'DocumentsController@documentsSent')->name('documents_sent');
Route::get('/documents', 'DocumentsController@allDocuments')->name('my_documents');
Route::get('/template/{templateId}', 'DocumentsController@previewTemplate')->name('preview_template');
Route::get('/document/{documentId}', 'DocumentsController@goToDocumentSigning')->name('preview_document');
Route::get('/templates', 'DocumentsController@allTemplates')->name('all_templates');
Route::any('/new-ticket', 'DocumentsController@newTicket')->name('new_ticket');


Route::webhooks('/docusign-webhook', 'docusign');
// Route::any('/docusign-webhook',WebhookController@onNewEvent')->name('docusign_webhook');



Route::any('/attach', 'HomeController@attachSample')->name('test-attach');

Route::get('/attachment/{attachmentId}', 'HomeController@viewAttachment')->name('attachment');

Route::get('/verify/phone', 'HomeController@verifyPhoneNo')->name('verify-phone');
Route::get('/verify/email', 'HomeController@verifyEmail')->name('verify-email');
